---
layout: sec_direction
title: "Category Direction - Permissions"
description: "GitLab's Permissions category provides both default roles and custom roles to enable organizations to set up least-privileged users across their GitLab environment."
canonical_path: "/direction/govern/authorization/permissions/"
---

- TOC
{:toc}

## Govern

| | |
| --- | --- |
| Stage | [Govern](/direction/govern/) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2024-02-08` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->
This direction page describes GitLab's plans for the permissions category that covers both [default roles](https://docs.gitlab.com/ee/user/permissions.html) and [custom roles](https://docs.gitlab.com/ee/user/custom_roles.html). This page belongs to the Authorization group and is maintained by the Product Manager, [Joe Randazzo](https://gitlab.com/jrandazzo).

This direction page is a work in progress, and everyone can contribute:

- Comment on existing [Authorization issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name[]=group%3A%3Aauthorization) in the public gitlab-org/gitlab issue tracker. If you don't see an issue that matches, file [a new issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature Proposal - basic), then post a comment that says @gitlab-bot label ~"group::authorization" ~"Category:Permissions" so your issue lands in our triage workflow.
- Please share feedback directly via [email](mailto:jrandazzo@gitlab.com) or schedule a[ video call](https://calendly.com/joerandazzo/30min). If you're a GitLab user and have direct knowledge of your need for permissions, default roles, custom roles, RBAC, or access management, we’d love to hear from you.

### Overview
<!-- Describe your category so that someone who is not familiar with the market space can understand what the product does. 
-->

The Permissions category has primarily been driven by [default roles](https://docs.gitlab.com/ee/user/permissions.html#roles) that include Guest, Reporter, Developer, Maintainer, and Owner. These permissions can be scoped to resources including groups, users, protected objects, and tokens. More recently, the release of [custom roles](https://docs.gitlab.com/ee/user/custom_roles.html) has enabled organizations that require strict permissions across various resources in GitLab. 

The Permissions category does not include Login, SAML, LDAP, Enterprise Users, User Management, or Tokens. These features can be found in the [Authentication group](https://about.gitlab.com/direction/govern/authentication/).

### Strategy and Themes
<!-- Capture the main problems to be solved in market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->

We want to help organizations by delivering controls to enable separation of duties and secure their SDLC within the GitLab Platform. While default roles can help 80% of organizations, another 20% operate in regulated environments that need tighter restrictions to resources in the platform. To achieve this, custom roles will fill the gap as a way to define RBAC to ensure granular permissions on resources. The driving principles around Authorization include:

- Principle of Least Privilege: Provide controls that eliminate unnecessary admin access, and ensure human or non-human users only have the privileges necessary to perform their duties.
- Access Transparency: Present insights to understand who has access to resources, potential overprivileged users, and gaps in separation of duties.
- Auditable Access: Ensure permissions and changes to user access are audited.

### 1 year plan
<!--
1-year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road map". Items in this section should be linked to issues or epics that are up to date. Indicate the relative priority of initiatives in this section so the audience understands the sequence in which you intend to work on them. 
 -->

In FY25, we are planning to focus on the following Product Themes:

[Drive Use Case Adoption to Fully Realize Value](https://about.gitlab.com/direction/#drive-use-case-adoption-to-fully-realize-value) : 

While security and governance drive value in Ultimate, custom roles and flexibility of permissions play a key role in adoption.

1. [Permissions for Custom Roles](https://gitlab.com/groups/gitlab-org/-/epics/12614): GitLab has a large customer base that operates in a heavily regulated environment who requires granular control over human and non-human access to resources within the platform. Customers have historically relied on [default roles](https://docs.gitlab.com/ee/user/permissions.html) to support user access but these permissions have fallen short due to inflexibility or being over privileged. To support these customer environments, we will be focusing on expanding [custom roles](https://docs.gitlab.com/ee/user/custom_roles.html) that map to common categories so an organization can adhere to the Principle of Least Privilege by defining RBAC. 
2. [Role Templates](https://gitlab.com/groups/gitlab-org/-/epics/12717): Templates can be used to simplify the custom role creation process by preselecting permissions of a default role, especially if few permissions only need to change. For templates to be usable, an inventory of permissions must be established. 
3. [Resource Access Analyzer](https://gitlab.com/groups/gitlab-org/-/epics/12718): Organizations perform user access reviews to identify overprivileged or unintended access to resources. These reviews are driven by compliance needs or standard security practices. Areas of review in the SDLC include role access to groups or projects, admin setting access, approval controls, protected branches, and environments.

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration you have planned for the category. This section must provide links to issues or
to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompassing a vision with a longer horizon, and don't lay out an iteration plan. -->

- See our [prioritized roadmap here](https://about.gitlab.com/direction/govern/authorization/).

<!--### Best in Class Landscape
 Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

<!-- 
<summary>BIC Roadmap</summary>
Key deliverables we're focusing on to build a BIC solution. List the epics by title and link to the epic in GitLab. Minimize additional description here so that the epics can remain the SSOT. This may be duplicative to the 1 year section however for some categories the key deliverables required to become the BIC solution will extend beyond one year and we want to capture all of the gaps. Moreover, the 1 year section may contain work that is not directly related to closing gaps if we are already the BIC or if we are differentiating ourselves.-->

### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.
Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
1. [Sidney (System Administrator)](https://handbook.gitlab.com/handbook/product/personas/#sidney-systems-administrator)
1. [Issac(Infrastructure Security Engineer)](https://handbook.gitlab.com/handbook/product/personas/#isaac-infrastructure-security-engineer)
1. [Delaney (Development Team Lead)](https://about.gitlab.com/handbook/product/personas/#delaney-development-team-lead)
1. [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/product/personas/#devon-devops-engineer)
1. [Alex (Security Operations Engineer)](https://about.gitlab.com/handbook/product/personas/#alex-security-operations-engineer)
1. [Parker (Product Manager)](https://about.gitlab.com/handbook/product/personas/#parker-product-manager)
1. [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/product/personas/#cameron-compliance-manager)


### Pricing and Packaging
<!-- 
-->

Default roles are available for all tiers while free Guest users are for the Ultimate tier only. 

Custom roles serve the need for Large Enterprise customers, Mid-Market customers, and those who operate in regulated industries such as Financial, Healthcare, or Public Sector. This feature is available for Ultimate tier only. 

<!-- ### Analyst Landscape -->


