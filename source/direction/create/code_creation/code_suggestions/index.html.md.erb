---
layout: sec_direction
title: "Category Direction - Code Suggestions"
description: "Code recommendations and suggestions within IDE editor extensions"
noindex: true
---

## On this page
{:.no_toc}

- TOC
{:toc}

# Code Suggestions

| | |
| --- | --- |
| Stage | [Create](/direction/create/) |
| Group | [Code Creation](/direction/code_creation) |
| Maturity | [minimal](/direction/maturity/) |
| Content Last Reviewed | `2024-01-30` |

### Introduction and how you can help

Thanks for visiting this category direction page on Code suggestions in GitLab. This page belongs to the [Code Creation](/..) group of the [Create stage](../create) and is maintained by Kevin Chu (kchu at gitlab dot com).

## Overview

AI Coding Assistants use [Generative AI](https://en.wikipedia.org/wiki/Generative_artificial_intelligence) to suggest relevant code snippets and autocomplete code as software developers type. Coding assistants aim to boost programmer productivity and reduce time spent on rote coding tasks. Some key points:

- AI Coding Assistants often use [large language models](https://en.wikipedia.org/wiki/Large_language_model), trained on huge data sets, such as open source code repositories or other scraped content, to repeatedly predict the next token or code snippet, provided a prompt.
- AI Coding Assistants should empower Developers to focus on higher logic and architecture rather than routine coding.
- Limitations and risks of using AI Coding Assistants include: 
  - Bias perpetuation - Gen AI may inadvertently amplify biases found in the training data.
  - Quality control - There is a risk of generating incorrect or faulty code without proper evaluation.
  - Legal - Copyright, attribution, and licensing for all AI-generated content need to be thought through.
- AI Coding Assistants today are largely integrated into IDEs, but code suggestion is not only limited to writing code in the IDE.  

### Vision

GitLab Duo Code Suggestions, part of the comprehensive AI-powered DevSecOps platform, empower increased coding productivity, without sacrificing security, privacy, and enterprise control.

### Strategy

GitLab Duo Code Suggestions is part of the larger suite of GitLab Duo, GitLab's AI-powered features and capabilities. The AI Coding Assistant category is among the most obvious (even if it is not the biggest SDLC efficiency driver) and mature among AI categories. By first empowering customers to adopt code suggestions, we can then jointly prioritize and augment the remaining DevSecOps workflow with AI, helping organizations to be fundamentally more efficient in bringing ideas from inception to production.

#### Short-term (3-6 months) Strategy

Having brought Code Suggestions to GA on 2023-12-21, our short-term (3 months) goal is to support the launch of [GitLab Duo Pro](https://about.gitlab.com/blog/2024/01/17/gitlab-duo-pro/), improve the fit and finish of the product, and bring critical enterprise controls online.

We need to continue to move quickly, as customers are continuing to actively invest in bringing AI into their software development workflows. We risk alienating customers who are looking to adopt AI if the quality of code suggestions lag behind other available products. By maturing code suggestions and bringing online other GitLab Duo capabilities, we can mitigate this risk while collaborating with customers to drive towards improved overall productivity.

#### Long-term Strategy

Longer-term, we plan to position code suggestions, along with the rest of our AI offerings, to differentiate on:

- AI across the software development life cycle
- Transparency and privacy first
- The right Model for each use case

##### AI across the software development life cycle

Traditionally the creation of code occurs in code editors or IDEs. However in its essence this is a conceptual work performed by trained individuals. By bringing code creation capabilities to other stages of software development life cycle (for example: to CI failed pipelines, or a vulnerability detected with a security scan), we aim to make creating code as easy as possible, empower more members to contribute and redefine perception of code creation as it exists Today.

##### Transparency and Privacy

Managing feature access at scale is an important problem to solve for enterprises. Furthermore, providing visibility into how code suggestions is used, how code suggestion impacts overall productivity will help GitLab customers make data-informed decisions.

Furthermore, Code is enterprise IP. How we enable privacy without compromise even as new AI-powered features become available is a top concern for organizations. Particularly for some GitLab self-manged or dedicated customers, having to send code to 3rd party models may not be a tenable proposition long term. We plan to investigate available options to retain complete privacy, including for air-gapped customers.

##### The right Model for each use case

The world of Gen AI Models, LLMs, SLMs, is quickly evolving. A [flexible design](https://docs.gitlab.com/ee/architecture/blueprints/ai_gateway/index.html) is at the core of GitLab Duo, it enables us to follow dynamically evolving landscape of AI tooling and select optimal models for a given use case. We plan to be granular such that we can use different models by language, deployment type, customization need for code suggestions.

#### Target Audience

People who code:

1. [Sasha (Software Developer)](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer)
1. [Priyanka (Platform Engineer)](https://about.gitlab.com/handbook/product/personas/#priyanka-platform-engineer)
1. [Simone (Software Engineer in Test)](https://about.gitlab.com/handbook/product/personas/#simone-software-engineer-in-test)

In the future, we may expand to include security personas to help write more secure code and review code for security vulnerabilities and fix them early in the software development lifecycle (SDLC), before you commit.

### 1-year plan

#### Roadmap

| Theme | Metric | Description | Tactics/Epics | Timing |
| ------ | ------ | ------ | ------ | ------ | 
| Enterprise Control | N/A | Enable Enterprise to Operate Code Suggestions with the appropriate controls | [AI Ban](https://gitlab.com/groups/gitlab-org/-/epics/12404), [Enterprise Seat Provisioning](https://gitlab.com/gitlab-org/gitlab/-/issues/439954), [Self-Service Purchasing](https://gitlab.com/groups/gitlab-org/-/epics/10336), [Improved User Provisioning](https://gitlab.com/groups/gitlab-org/-/epics/10510) | FY25-Q1 | 
| Usability | % of errors, Acceptance Rate, user growth | Fix and improve UX issues | [Improved Language Support](https://gitlab.com/groups/gitlab-org/editor-extensions/-/epics/4), [Reduce error requests](https://gitlab.com/groups/gitlab-org/-/epics/11784), [Streaming across all IDEs](https://gitlab.com/groups/gitlab-org/-/epics/11722) | FY25-Q1, FY25-Q2 | 
| Trigger Logic | Shown Rate, Acceptance Rate | Improve when code suggestions are requested | [Improve Trigger Logic](https://gitlab.com/groups/gitlab-org/-/epics/12100), [Reduce code suggestion requests](https://gitlab.com/groups/gitlab-org/-/epics/11662)  | FY25-Q1, FY25-Q2 |
| Decrease latency | Load Time, Acceptance Rate | Decrease the latency between when a suggestion is requested and shown to the user | [Code Suggestions Performance Improvements](https://gitlab.com/groups/gitlab-org/-/epics/12160),[Architecture Update](https://gitlab.com/groups/gitlab-org/-/epics/12224) | FY25-Q1, FY25-Q2 |
| Increase SM/Dedicated upgrade cycle | | Minimize the frequency GitLab instances have to be updated in order to get improvements in Code Suggestions and other Duo features | [Decouple Code Suggestion Improvements from GitLab Updates](https://gitlab.com/groups/gitlab-org/-/epics/12588) | FY25-Q3 |
| Increased context | Acceptance Rate | Increase the context so that more relevant results are returned | [RAG](https://gitlab.com/groups/gitlab-org/-/epics/11669) | FY25 |
| ROI Proof | | Establish credible and meaningful metric(s) that customers can access directly | [ROI Proof](https://gitlab.com/groups/gitlab-org/-/epics/12590) | FY25 |

### What we recently completed

1. Performance improvements
1. X-ray of 3rd party libraries
1. Code Tasks in Duo Chat
1. Use Anthropic for Code Generation

### What is Not Planned Right Now

- **Fully original GitLab-trained model**: We can iterate faster now by partnering with best-in-class LLM vendors.
- **Generating code outside of the IDE**: While we may eventually allow developers to generate code outside of the IDE, we are starting with code generation only in the IDE.

#### Maturity Plan

This category is currently at minimal maturity.

### User success metrics

- **Acceptance Rate** - This metric is currently the primary metric for quality. We assume that if the suggestion is relevant, timely, and helps the developer, the more likely the developers are to accept the suggestion. 
- **Developer cycle time** - Code Suggestions may make developers more efficient and we believe help reduce the number of bugs, security vulnerabilities, and stylistic issues improving developer cycle time.
- **Time to merge** - With more efficient coding, merge requests will merge faster with less review time and back and forth between reviewers and authors.

## Competitive Landscape

Please see the content in our [internal handbook](https://internal.gitlab.com/handbook/product/best-in-class/modelops)
