---
features:
  top:
  - name: "GitLab Duo Chat Beta now available in Premium"
    available_in: [premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/gitlab_duo_chat.html'
    image_url: '/images/16_9/gitlab_duo_chat_beta_now_available_in_premium.png'
    reporter: tlinz
    stage: ai-powered
    categories:
    - Duo Chat
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/11251'
    description: |
      In 16.8, we made GitLab Duo Chat available for self-managed instances. In 16.9, we are making Chat available to Premium customers while it is still in Beta.

      GitLab Duo Chat can:

      * Explain or summarize issues, epics, and code.
      * Answer specific questions about these artifacts like "Collect all the arguments raised in comments regarding the solution proposed in this issue."
      * Generate code or content based on the information in these artifacts. For example, "Can you write documentation for this code?"
      * Help you start a process. For example, "Create a .gitlab-ci.yml configuration file for testing and building a Ruby on Rails application in a GitLab CI/CD pipeline."
      * Answer all your DevSecOps related question, whether you are a beginner or an expert. For example, "How can I set up Dynamic Application Security Testing for a REST API?"
      * Answer follow-up questions so you can iteratively work through all the previous scenarios.

      GitLab Duo Chat is available as a [Beta](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta) feature. It is also integrated into our Web IDE and GitLab Workflow extension for VS Code as [Experimental](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#experiment) features. In these IDEs, you can also use [predefined chat commands that help you do standard tasks more quickly](https://docs.gitlab.com/ee/user/gitlab_duo_chat_examples.html#explain-code-in-the-ide) like writing tests.

      You can help us mature these features by providing feedback about your experiences with GitLab Duo Chat, either within the product or through our [feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/430124).
