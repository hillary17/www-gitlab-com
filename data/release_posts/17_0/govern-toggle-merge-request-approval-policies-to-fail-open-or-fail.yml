---
features:
  secondary:
  - name: "Toggle merge request approval policies to fail open or fail closed"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html#fallback_behavior'
    image_url: '/images/17_0/toggle-fail-open.png'
    reporter: g.hickman
    stage: govern
    categories:
    - Security Policy Management
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/10816'
    description: |
      Compliance operates on a sliding scale for many organizations as they strike a balance between meeting requirements and ensuring developer velocity is not impacted. Merge request approval policies help to operationalize security and compliance in the heart of the DevSecOps workflow - the merge request. We're introducing a new `fail open` option for merge request approval policies to offer flexibility to teams who want to ease the transition to policy enforcement as they roll out controls in their organization.

      When a merge request approval policy is configured to fail open, MRs will now only be blocked if a policy rule is violated **and** if that project has the security analyzer properly configured. If an analyzer is not enabled for a project or if the analyzer does not successfully produce results, the policy will no longer consider this a violation for the given rule and analyzer. This approach allows for progressive rollout of policies as teams work to ensure proper scan execution and enforcement.
