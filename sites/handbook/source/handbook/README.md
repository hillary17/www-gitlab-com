# The Handbook has been migrated

The handbook has been migrated to the new handbook site.  We've removed all the content from here and the Single Source of Truth can be found at:

- [The content-sites/handbook gitlab Repo](https://gitlab.com/gitlab-com/content-sites/handbook)
- [The new handbook site](https://handbook.gitlab.com)

If you need help or assistance with this please reach out to @jamiemaynard (Develop/Handbooks).  Alternatively ask your questions on slack in [#handbook](https://gitlab.slack.com/archives/C81PT2ALD)
